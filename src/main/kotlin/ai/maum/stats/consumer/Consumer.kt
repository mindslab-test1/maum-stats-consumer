package ai.maum.stats.consumer

import io.lettuce.core.RedisBusyException
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.ProducerConfig
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.data.redis.RedisSystemException
import org.springframework.data.redis.connection.stream.StreamRecords
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.support.KafkaHeaders
import org.springframework.messaging.MessageHeaders
import org.springframework.messaging.handler.annotation.Header
import org.springframework.messaging.handler.annotation.Headers
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import java.text.SimpleDateFormat
import java.util.*
import javax.annotation.PostConstruct

@Profile("!test")
@Component
class Consumer(
        var redisTemplate: StringRedisTemplate
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private var consumer: KafkaConsumer<String?, String?>? = null

    @org.springframework.beans.factory.annotation.Value("\${spring.kafka.bootstrap-servers}")
    private val bootstrapServer: String? = null

    @org.springframework.beans.factory.annotation.Value("\${spring.kafka.consumer.group-id}")
    private val groupID: String? = null

    @org.springframework.beans.factory.annotation.Value("\${spring.kafka.consumer.key-deserializer}")
    private val keyDeSerializer: String? = null

    @org.springframework.beans.factory.annotation.Value("\${spring.kafka.consumer.value-deserializer}")
    private val valueDeSerializer: String? = null

    @org.springframework.beans.factory.annotation.Value("\${spring.kafka.consumer.auto-offset-reset}")
    private val offsetReset: String? = null

    @org.springframework.beans.factory.annotation.Value("\${spring.kafka.template.default-topic}")
    private val topicName: String? = null

    @org.springframework.beans.factory.annotation.Value("\${spring.kafka.consumer.max-poll-records}")
    private val maxPollRecords: String? = null

    @org.springframework.beans.factory.annotation.Value("\${spring.kafka.consumer.enable-auto-commit}")
    private val enableAutoCommit: String? = null

    @PostConstruct
    fun build() {
        val properties = Properties()
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer)
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupID)
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, keyDeSerializer)
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, valueDeSerializer)
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, offsetReset)
        properties.setProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, maxPollRecords)
        properties.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, enableAutoCommit)
        consumer = KafkaConsumer(properties)

        val sop = redisTemplate.opsForStream<String, String>()

        logger.info("[${this.javaClass}] post-construct create group result: " +
                try{
                    sop.createGroup("ai.maum.stats.kafka.topic.api-usage", "ai.maum.stats.reader")
                } catch (e: RedisSystemException){
                    val rootCause = e.rootCause
                    if (rootCause is RedisBusyException){
                        val message = rootCause.message ?: throw rootCause
                        if(message.contains("Consumer Group name already exists")) "exists" else throw rootCause
                    } else{
                        throw e.rootCause ?: e
                    }
                })
        logger.info("[${this.javaClass}] post-construct create group result: " +
                try{
                    sop.createGroup("ai.maum.stats.kafka.topic.statistics", "ai.maum.stats.reader")
                } catch (e: RedisSystemException){
                    val rootCause = e.rootCause
                    if (rootCause is RedisBusyException){
                        val message = rootCause.message ?: throw rootCause
                        if(message.contains("Consumer Group name already exists")) "exists" else throw rootCause
                    } else{
                        throw e.rootCause ?: e
                    }
                })
    }

    @KafkaListener(topics = [
        "\${spring.kafka.template.default-topic}",
        "ai.maum.stats.kafka.topic.api-usage",
        "ai.maum.stats.kafka.topic.statistics"
    ])
    fun consume(@Headers headers: MessageHeaders, @Header(KafkaHeaders.RECEIVED_TOPIC) topic: String, @Payload payload: String) {
        val uuid = UUID.randomUUID()

        try {
            logger.info("[$uuid] consume header: $headers")
            logger.debug("[$uuid] consume payload: $payload")

            val sop = redisTemplate.opsForStream<String, String>()
            val record = StreamRecords.newRecord().`in`(topic)
                    //.withId("") // skip to use auto-generated id
                    .ofObject(payload)
            sop.add(record)

            logger.info("[$uuid] message sent to redis successfully")
        } catch (e: Exception) {
            logger.error("[$uuid] ${e.message}")
            logger.debug("[$uuid] ${e.stackTraceToString()}")
        }
    }
}