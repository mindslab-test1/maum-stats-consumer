import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

// Kotlinx dependencies
val serializationVersion = "1.0.1"
val coroutinesVersion = "1.4.1"

plugins {
    application
    id("org.springframework.boot") version "2.4.0"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    kotlin("jvm") version "1.4.10"
    kotlin("plugin.spring") version "1.4.10"
    kotlin("plugin.serialization") version "1.4.10"

    // It is a necessary plug-in for an optimized Docker image configuration.
    // Reference Link: https://github.com/palantir/gradle-docker
    id("com.palantir.docker") version "0.22.1"
}

group = "ai.maum.stats"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11
application.mainClassName = "ai.maum.stats.consumer.ConsumerApplicationKt"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-redis")
    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.kafka:spring-kafka")

    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$serializationVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")

    // async logging
    implementation("com.lmax:disruptor:3.4.2")

    implementation("org.apache.commons:commons-pool2:2.6.2")
    implementation("it.ozimov:embedded-redis:0.7.3")

    runtimeOnly("com.h2database:h2")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.kafka:spring-kafka-test")
}

configurations {
    all {
//        exclude(group = "org.springframework.boot", module = "spring-boot-starter-json")
        exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.create<Copy>("unpack") {
    dependsOn(tasks.bootJar)
    from(zipTree(tasks.bootJar.get().outputs.files.singleFile))
    into("build/dependency")
}

docker {
    name = "maum-stats-consumer"
    copySpec.from(tasks.findByName("unpack")?.outputs).into("dependency")
    buildArgs(mapOf("DEPENDENCY" to "dependency"))
}
